from flask import Flask, redirect, url_for, render_template, request
import time
import datetime, requests,json


app =Flask(__name__)

@app.route("/", methods=['GET'])
def home():
	return render_template("index.html")

@app.route("/", methods=['POST'])
def sendEmail():
	headers = {
    'content-type':'application/json',
    'api-key': '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',
    'accept': 'application/json'
	}
	customerName=request.form['fullName']
	customerEmail=request.form['email']
	customerPhone=request.form['phone']
	customerMessage=request.form['message']
	html="""<html><head></head><body>
	   					<p>Hello Haris,<br>
	   						You have received a inquiry from your flask demo web.<br>
	   						Name  :"""+customerEmail+"""<br>
	   						Email  :"""+customerName+"""<br>
	   						Phone  :"""+customerPhone+"""<br>
	   						Message  :"""+customerMessage+"""<br>
	   					</p>.</p></body></html>"""
	data = {  
	   "sender":{  
	      "name":"Haris Haider",
	      "email":"harisflaskdemoweb@test.com"
	   },
	   "to":[  
	      {  
	         "email":"eharishaider@gmail.com",
	         "name":"Haris Haider"
	      }
	   ],
	   "subject":"Inquiry Come From Flask Demo Web",
	   "htmlContent":html}
	data=json.dumps(data, indent = 4)
	dataTo = {  
	   "sender":{  
	      "name":"Haris Haider",
	      "email":"harisflaskdemoweb@test.com"
	   },
	   "to":[  
	      {  
	         "email":customerEmail,
	         "name":customerName
	      }
	   ],
	   "subject":"Inquiry Come From Flask Demo Web",
	   "htmlContent":"""<html><head></head><body>
	   					<p>Hello """+customerName+""",<br>
	   						Thank you contacting us we will connect with shortly Thanks.<br>
	   						Regards<br/>
	   						Haris Haider Flask Demo
	   					</p>.</p></body></html>"""}
	dataTo=json.dumps(dataTo, indent = 4)
	response = requests.post('https://api.sendinblue.com/v3/smtp/email', headers=headers, data=data)
	response1 = requests.post('https://api.sendinblue.com/v3/smtp/email', headers=headers, data=dataTo)
	response.json();
	response1.json();
	return redirect(url_for('home'))
	# return render_template("index.html")


if __name__=="__main__":
	app.run(debug=True)
